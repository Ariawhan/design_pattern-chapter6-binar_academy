// Updated by Ariawan Putra

const { Post } = require("../models");

module.exports = {
  //list
  getAllPosts() {
    return Post.findAll();
  },
  //create
  createPost(title, body) {
    // console.log(body);
    return Post.create({
      title,
      body,
    });
  },
  //update
  updatePost(data, updateData) {
    console.log(data, updateData);
    return data.update(updateData);
  },
  //setPost
  choosePost(data) {
    // console.log(data);
    return Post.findByPk(data);
  },
  //destroy
  destroyPost(data) {
    // console.log(data);
    return data.destroy();
  },
};
