/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
// const { Post } = require("../../../models");
const { post } = require("../../../services/");

module.exports = {
  list(req, res) {
    post
      .getAllPosts()
      .then((posts) => {
        res.status(200).json({
          status: "ok",
          data: {
            posts,
          },
        });
      })
      .catch((e) => {
        res.status(400).json({
          status: "FAIL",
          message: "post failed to get",
        });
      });
  },

  create(req, res) {
    const { title, body } = req.body;
    post
      .createPost(title, body)
      .then((post) => {
        res.status(201).json({
          status: "Data berhasil ditambahkan",
          data: post,
        });
      })
      .catch((e) => {
        console.log(e);
        res.status(201).json({
          status: "FAIL",
          message: e.message,
        });
      });
  },

  update(req, res) {
    const postData = req.post;
    post
      .updatePost(postData, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
          data: postData,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const post = req.post;
    res.status(200).json({
      status: "OK",
      data: post,
    });
  },

  destroy(req, res) {
    post
      .destroyPost(req.post)
      .then(() => {
        // res.status(204).end();
        // console.log(req.post);
        res.status(206).json({
          status: "Id : " + req.post.id + "  Deleted",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setPost(req, res, next) {
    post
      .choosePost(req.params.id)
      .then((post) => {
        if (!post) {
          res.status(404).json({
            status: "FAIL",
            message: "Post not found!",
          });

          return;
        }

        req.post = post;
        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Post not found!",
        });
      });
  },
};
